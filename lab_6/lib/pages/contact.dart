import 'package:flutter/material.dart';
import './contact_add.dart';

class Contact extends StatelessWidget {
  final data = [
    {
      "name": "Said Abdurrahman",
      "email": "said.abdurrahman@ui.ac.id",
      "role": "Asdos",
      "matkul": "PBP",
    },
    {
      "name": "Putri Martha",
      "email": "putri.martha@ui.ac.id",
      "role": "Asdos",
      "matkul": "PBP",
    },
    {
      "name": "Alif Sulaiman",
      "email": "Alif.Sulaiman@ui.ac.id",
      "role": "Dosen",
      "matkul": "PBP",
    },
    {
      "name": "Kevin",
      "email": "razaqakevin@gmail.com",
      "role": "Dosen",
      "matkul": "PBP",
    },
    {
      "name": "Dandi Saputra",
      "email": "dandi.saputra@ui.ac.id",
      "role": "Dosen",
      "matkul": "Aljabar Linear",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Contact List')),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.grey[600],
        child: Text('+', style: TextStyle(fontSize: 35)),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ContactAdd()));
        },
      ),
      body: Container(
          margin: EdgeInsets.all(10),
          child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, i) {
              return Card(
                elevation: 8,
                child: ListTile(
                  title: Text(data[i]["name"].toString()),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    Text(data[i]["email"].toString()),
                    Text(data[i]["matkul"].toString()),
                  ],),
                  trailing: Text(data[i]["role"].toString()),
                ),
              );
            },
          )),
    );
  }
}
