from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from lab_2.models import Note



# Create your views here.

def index(request):
    notes = Note.objects.all().values()  # TODO Implement this
    response = {'notes' : notes}
    return render(request, 'lab2.html', response)

def xml(request):
    notes = Note.objects.all().values()  # TODO Implement this
    data = serializers.serialize('xml', notes)
    return HttpResponse(data, content_type="application/xml")

def json(request):
    notes = Note.objects.all().values()  # TODO Implement this
    data = serializers.serialize('json', notes)
    return HttpResponse(data, content_type="application/json")
