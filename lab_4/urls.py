from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('add-note/', add_NoteForm, name='add_note'),
    path('note-list/', note_list, name='note_list'),
]